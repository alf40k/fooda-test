package com.fooda;

/**
 * Class used for interpreting a Twitter Bearer token using GSON
 */
@SuppressWarnings({"CanBeFinal", "unused"})
class TwitterBearerToken {
    private String token_type;
    private String access_token;


    /**
     * Standard to string method. Displays token in the proper format to send as an Authorization token.
     */
    @Override
    public String toString() {
        return token_type + " " + access_token;
    }
}
