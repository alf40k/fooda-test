package com.fooda;

/**
 * Class used for interpreting a request error using GSON
 */
@SuppressWarnings({"unused", "CanBeFinal"})
class JSONError {
    @SuppressWarnings("unused")
    private String code;
    private String message;

    /**
     * Standard to string method. Displays the error code and message.
     */
    @Override
    public String toString() {
        return "code: " + code + "\nmessage:" + message;
    }
}
