package com.fooda;
// java.io imports
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// java.net imports
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

//java.util imports
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;

// GSON imports
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * <h1>Fooda Twitter Challenge Tool</h1>
 * The my_tool program is a basic twitter lookup tool. The tool is designed to take in
 * a twitter user and a number of tweets then display that number of tweets from the supplied user.
 * <br><br>
 * Usage: <code> my_tool user count</code>
 *
 * @author  Arthur Lunn
 * @version 1.0
 * @since   2017-05-21*
 */
class my_tool {

    private final static String USER_AGENT = "Mozilla/5.0";
    private final static String CONTENT_TYPE = "application/x-www-form-urlencoded;charset=UTF-8";
    private final static String POST_BODY = "grant_type=client_credentials";
    private final static String POST_URL = "https://api.twitter.com/oauth2/token";
    private final static String SEARCH_URL = "https://api.twitter.com/1.1/search/tweets.json?";
    private final static String USER_GET_URL = "https://api.twitter.com/1.1/users/lookup.json?";
    private final static String URL_EXCEPTION = "There was an error with the provided url: ";
    private final static String IO_EXCEPTION = "There was an IO error while attempting to make a connection: ";
    private final static String USAGE = "usage: my_tool user count";
    private final static String ARGS_ERROR = "Invalid number of arguments supplied";
    private final static String USER_ERROR = "User must be a twitter user in the form \"@user\"";
    private final static String COUNT_ERROR = "Count must be an integer between 1-100 inclusive";
    private final static String CONSUMER_KEY = "60BaU3LvdDlMYhomQyxNBO5Yf";
    private final static String CONSUMER_SECRET = "Ei34KFQ06RA5QCQEbseR7iSOhrAucHciR00Pdla9TwCRFR6llu";
    private final static RESTHelper helper = new RESTHelper();
    private final static Gson gson = new GsonBuilder().create();

    /**
     *
     * @param args [0] A twitter user [1] count of tweets to display
     */
    public static void main(String[] args) {
        // Initialize Input Steam Reader; this reader is used for both the POST and GET responses
        InputStreamReader in = null;
        String url;
        String user;
        TweetList tweets;
        String postAuth;
        String encodedKey;
        String encodedSecret;
        String tokenCredentials;
        int postDataLength;
        byte[] postData;
        int count;
        // Encode the key and secret into the authorization token
        encodedKey = encode1738(CONSUMER_KEY);
        encodedSecret = encode1738(CONSUMER_SECRET);
        tokenCredentials = encodedKey + ":" + encodedSecret;
        postAuth = encodeBase64(tokenCredentials);

        // Handle user arguments
        argCheck(args);
        user = userCheck(args[0]);
        count = countCheck(args[1]);

        // Handle conversion of POST_BODY data and get data length
        postData = new byte[0];
        try {
            postData = POST_BODY.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.exit(-2);
        }
        postDataLength = postData.length;
        // Create a hash map containing all of the header properties for the POST request
        HashMap<String, String> postProperties = setProperties("Basic " + postAuth, Integer.toString(postDataLength));

        // Attempt to make the POST request and handle any errors
        try {
            in = helper.sendPost(postData, POST_URL, postProperties);
        } catch (MalformedURLException e){
            System.err.println(URL_EXCEPTION);
            e.printStackTrace();
            System.exit(-3);
        } catch (IOException e){
            System.err.println(IO_EXCEPTION);
            e.printStackTrace();
            System.exit(-4);
        }

        // Parse the returned JSON into a bearer token handling any potential errors
        TwitterBearerToken token = null;
        if(in != null) {
            token = gson.fromJson(in, TwitterBearerToken.class);
        }
        else{
            System.err.println("Issue retrieving input stream from POST request");
            System.exit(-5);
        }

        // Close the input stream
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-6);
        }
        // Create a hash map containing all of the header properties for the GET request
        HashMap<String, String> getProperties = setProperties(token.toString(), null);


        // Assemble the GET URL to check if the user is valid from the user supplied argument data
        url = USER_GET_URL + "screen_name="+ user;
        in = sendGet(url, getProperties);
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-7);
        }

        // Assemble the GET URL to lookup tweets from the user supplied argument data
        url = SEARCH_URL + "q=from:"+ user + "&count=" + count;
        in = sendGet(url, getProperties);

        // Parse the JSON to a TweetList
        tweets = gson.fromJson(in, TweetList.class);

        //print result
        if(tweets.getTweets().isEmpty()){
            System.out.println("This user has not tweeted in the past week");
        }
        else{
            tweets.getTweets().forEach(System.out::println);
        }
        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-8);
        }
    }

    /**
     * Basic utility for testing. Takes an <code>InputStreamReader</code> and prints the contents of the stream
     * to <code>System.Out</code>.
     * @param in InputSteamReader to read from.
     */
    @SuppressWarnings("unused")
    public static void printInputStreamReader(InputStreamReader in){
        BufferedReader inReader = new BufferedReader(in);
        String line;
        try {
            while ((line = inReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (Exception e){
            System.out.println("Error reading from buffered reader:" + e);
        }
    }

    /**
     * Intermediary method for sending GET requests. Relies on a RESTHelper instance to
     * @param url URL to send the request to
     * @param properties Collection of request properties
     * @return The InputStream from the request
     */
    private static InputStreamReader sendGet(String url, HashMap<String, String> properties){
        InputStreamReader in = null;
        try {
            in = helper.sendGet( url, properties);
        } catch (MalformedURLException e){
            System.err.println(URL_EXCEPTION);
            e.printStackTrace();
        } catch (IOException e){
            System.err.println(IO_EXCEPTION);
            e.printStackTrace();
        }
        return in;
    }

    /**
     * Basic function for encoding to the 1738 standard
     * @param str String to be encoded
     * @return Encoded string
     */
    private static String encode1738(String str){
        String ret ="";
        try {
            ret = URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Basic function for encoding to Base64
     * @param str String to be encoded
     * @return Encoded string
     */
    private static String encodeBase64(String str){
        String ret = "";
        Encoder baseEncoder =  Base64.getEncoder();
        try {
            ret = baseEncoder.encodeToString(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ret;
    }

    /**
     * Function used to set the request properties of an HTTP request.
     * @param token Authorization token
     * @param postDataLength If request is post, give it's length
     * @return The Hashmap of request properties
     */
    private static HashMap<String, String> setProperties(String token, String postDataLength){
        HashMap<String, String> properties = new HashMap<>(3);
        properties.put("Authorization", token);
        properties.put("Content-Type", CONTENT_TYPE);
        properties.put( "User-Agent", USER_AGENT);
        if(postDataLength != null){
            properties.put( "Content-Length", postDataLength);
        }
        return properties;
    }

    /**
     * Check that the correct number of arguments were supplied
     * @param args CLI arguments
     */
    private static void argCheck(String[] args){
        if(args.length != 2){
            System.err.println(ARGS_ERROR);
            System.err.println(USAGE);
            System.exit(-1);
        }
    }

    /**
     * Check that the user argument supplied is valid
     * @param _user argument to check
     * @return parsed user
     */
    private static String userCheck(String _user){
        String user = _user;
        if(user.charAt(0) == '@'){
            user = user.substring(1);
        }
        else{
            System.err.println(USER_ERROR);
            System.err.println(USAGE);
            System.exit(-1);
        }
        return user;
    }

    /**
     * Check that the count argument supplied is valid
     * @param _count argument to check
     * @return parsed count
     */
    private static int countCheck(String _count){
        int count = -1;
        try{
            count = Integer.parseInt(_count);
        } catch (NumberFormatException e){
            System.err.println(COUNT_ERROR);
            System.err.println(USAGE);
            System.exit(-1);
        }
        if(count < 1 || count > 100){
            System.err.println(COUNT_ERROR);
            System.err.println(USAGE);
            System.exit(-1);
        }
        return count;
    }
}
