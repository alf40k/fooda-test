package com.fooda;

/**
 * Class used for interpreting tweets using GSON
 */
@SuppressWarnings({"CanBeFinal", "unused"})
class Tweet {
    private String created_at;
    private Object current_user_retweet;
    private int favorite_count;
    private boolean favorited;
    private String filter_level;
    private Object geo;
    private long id;
    private String id_str;
    private String in_reply_to_screen_name;
    private String in_reply_to_status_id_str;
    private Long in_reply_to_user_id;
    private String in_reply_to_user_id_str;
    private String lang;
    private boolean possibly_sensitive;
    private long quoted_status_id;
    private String quoted_status_id_str;
    private Object scopes;
    private int retweet_count;
    private boolean retweeted;
    private String source;
    private String text;
    private boolean truncated;
    private TwitterUser user;
    private boolean withheld_copyright;
    private String[] withheld_in_countries;
    private String withheld_scope;

    /**
     * Standard to string method. Tweets are formatted as a 150x5 box displaying the author, the time written,
     * as well as the tweet text, favorite count and retweets count.
     */
    @Override
    public String toString() {
        String rs = "";
        rs = rs.concat(String.format("\n\t%1$s tweeted at %2$-112s \n%3$-145s\n\n\tFavorites: %4$-20sRetweets: %5$-20s\n",
                user, created_at, text, favorite_count , retweet_count));
        return rs;
    }
}
