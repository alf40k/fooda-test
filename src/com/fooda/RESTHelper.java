package com.fooda;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Helper class used to send GET and POST requests.
 */
class RESTHelper {

    /**
     * Helper method designed to handle POST HTTP requests. User must supply data to post, a url to post to, and
     * any request properties that are going to be used. Errors in opening the connection are thrown and
     * errors with the response codes or getting the input stream are caught within the function call.
     *
     * @param postData raw byte representation of the POST data body
     * @param url The url to POST the request to
     * @param properties The request header properties
     * @return InputStreamReader of the request input data
     * @throws IOException If there are issues establishing the HttpURLConnection an IOException will be thrown
     */
    InputStreamReader sendPost(byte[] postData, @SuppressWarnings("SameParameterValue") String url, HashMap<String, String> properties) throws IOException{
        int responseCode = -1;
        URL obj = new URL(url);
        Gson gson = new GsonBuilder().create();
        // Establish the initial connection and set parameters
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        properties.forEach(connection::setRequestProperty);
        connection.setInstanceFollowRedirects( false );
        connection.setUseCaches( false );
        // Write the post data to the connection
        DataOutputStream wr = new DataOutputStream( connection.getOutputStream());
        wr.write( postData );
        wr.close();
        // Get the response data from the connection
        InputStreamReader in = null;
        try {
            responseCode = connection.getResponseCode();
        } catch (Exception e){
            System.err.println(e.toString());
        }
        try {
            in = new InputStreamReader(connection.getInputStream());
        } catch (Exception e){
            System.err.println(e.toString());
        }
        // Handle any unsuccessful response codes
        if(responseCode != 200){
            in = new InputStreamReader(connection.getErrorStream());
            System.err.println("The POST request was not successful:" + responseCode);
            JSONErrorList errors = gson.fromJson(in, JSONErrorList.class);
            errors.getErrors().forEach(System.err::println);
            System.exit(-1);
        }
        return in;
    }

    /**
     * Helper method designed to handle GET HTTP requests. User must supply a url to GET from, and
     * any request properties that are going to be used. Errors in opening the connection are thrown and
     * errors with the response codes or getting the input stream are caught within the function call.
     *
     * @param url The url to GET the request from
     * @param properties The request header properties
     * @return InputStreamReader of the request input data
     * @throws IOException If there are issues establishing the HttpURLConnection an IOException will be thrown
     */
    InputStreamReader sendGet(String url, HashMap<String, String> properties)  throws IOException{
        int responseCode = -1;
        URL obj = new URL(url);
        Gson gson = new GsonBuilder().create();
        // Establish the initial connection and set parameters
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setDoOutput(false);
        connection.setDoInput(true);
        connection.setRequestMethod("GET");
        connection.setInstanceFollowRedirects( false );
        properties.forEach(connection::setRequestProperty);
        connection.setUseCaches( false );
        // Get the response data from the connection
        InputStreamReader in = null;
        try {
            responseCode = connection.getResponseCode();
        } catch (Exception e){
            System.err.println(e.toString());
        }
        try {
            in = new InputStreamReader(connection.getInputStream());
        } catch (Exception e){
            System.err.println(e.toString());
        }
        // Handle any unsuccessful response codes
        if(responseCode != 200){
            in = new InputStreamReader(connection.getErrorStream());
            System.err.println("The GET request was not successful:" + responseCode);
            JSONErrorList errors = gson.fromJson(in, JSONErrorList.class);
            errors.getErrors().forEach(System.err::println);
            System.exit(-1);
        }
        return in;
    }
}
