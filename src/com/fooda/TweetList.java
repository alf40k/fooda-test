package com.fooda;

import java.util.List;

/**
 * Class used for interpreting lists of tweets using GSON
 */
@SuppressWarnings("unused")
class TweetList {
    @SuppressWarnings("CanBeFinal")
    private List<Tweet> statuses;

    /**
     * Standard getter for errors list
     * @return list of errors
     */
    public List<Tweet> getTweets(){
        return statuses;
    }
}
