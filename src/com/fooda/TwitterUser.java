package com.fooda;

/**
 * Class used for interpreting a twitter user using GSON
 */
@SuppressWarnings({"CanBeFinal", "unused"})
class TwitterUser {
    private boolean contributors_enabled;
    private String created_at;
    private boolean default_profile;
    private boolean default_profile_image;
    private String description;
    private int favourites_count;
    private int followers_count;
    private int friends_count;
    private boolean geo_enabled;
    private long id;
    private String id_str;
    private boolean is_translator;
    private String lang;
    private int listed_count;
    private String location;
    private String name;
    private String screen_name;

    /**
     * Standard to string method. Displays the users screen name.
     */
    @Override
    public String toString() {
        return screen_name;
    }
}
