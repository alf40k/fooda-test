package com.fooda;

import java.util.List;

/**
 * Class used for interpreting a list of request errors using GSON
 */
@SuppressWarnings({"unused", "CanBeFinal"})
class JSONErrorList {
    private List<JSONError> errors;

    /**
     * Standard getter for errors list
     * @return list of errors
     */
    public List<JSONError> getErrors(){
        return errors;
    }
}
