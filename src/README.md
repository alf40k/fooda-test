# Fooda Internship Challenge

## Assumptions
I assumed that the project should be handled using as few dependencies as possible. For this reason I made an effort to stick to native java dependencies. If this weren't the case it would have been easy to complete this challenge using a [Twitter library](https://dev.twitter.com/resources/twitter-libraries).

In the case of [GSON](https://sites.google.com/site/gson/gson-user-guide) I felt it was reasonable to use the dependency as designing a tool to efficiently parse JSON felt out of the scope of the project. In addition this is a fairly standard tool and it gave me an opportunity to show that I can work with dependencies as needed.

I assumed that you did not want me to hard code a bearer token as there could be potential issues with either security or forward compatibility.

I assumed that a range of 0-100 would be good since we're using the REST API and not the streaming API.

I assumed since there is no language specified for the challenge that not being able to run the program with a literal `my_tool @gofooda 100` but instead by navigating to the JAR at `fooda_test/out/artifacts/my_tool_jar` and using the command `java -jar my_tool.jar @gofooda 100` would be acceptable.

## Notes

*Here I've included some of the notes I had for myself while coming up with a solution.*

- JSON parsing? -> Handled with GSON instead of building a parsing solution.

- TLS Support? -> requests sent over HTTPS seem to be working

- Error code 99 occurred when attempting to secure a bearer token while also logged into twitter through Postman, troubleshoot if this is an issue in the CLI version. - > Only a problem with PostMan, CLI doesn't have this issue

## Steps for APP

1. Read user entry from command line handling any input errors. If the incorrect number of arguments given, print error and exit gracefully.

2. Parse user arguments into commands for the application. If invalid arguments are given close gracefully

3. encode consumer key and secret into a specially encoded set of credentials.

4. Send HTTP POST request to the POST oauth2/token endpoint to exchange these credentials for a bearer token.

5. From here on the application uses the bearer token to authenticate.

6. Send HTTP Get request to the GET search/tweets endpoint using user supplied query data parsed from arguments.

7. Using the InputStreamReader received from connection and GSON parse the returned tweet data into a list of tweets.

8. Format and output the list of tweets.

